package com.adaptionsoft.games.trivia

import java.util.*

class FixedRandom: Random() {
    private val fixedRandoms = mutableListOf(0, 2, 1, 2, 2, 7, 1, 2, 1, 2, 3, 5, 1, 1, 2, 6, 0, 0, 0, 1, 1, 6, 1, 7, 3, 1, 3, 5, 1, 7, 2, 2)

    override fun nextInt(bound: Int): Int {
        return fixedRandoms.removeFirst()
    }
}