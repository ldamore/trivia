package com.adaptionsoft.games.trivia

import com.adaptionsoft.games.trivia.runner.runMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.ByteArrayOutputStream
import java.io.PrintStream


class TriviaTest {
    @Test
    fun `golden master`() {
        val fixedRandom = FixedRandom()

        runMain(fixedRandom)

        assertThat(outputStreamCaptor.toString()).isEqualTo(readGoldenMaster())
    }

    @BeforeEach
    fun setUp() {
        System.setOut(PrintStream(outputStreamCaptor))
    }

    @AfterEach
    fun tearDown() {
        System.setOut(standardOut)
    }

    private fun readGoldenMaster(): String = ClassLoader.getSystemResource("trivia.txt").readText()

    private val standardOut = System.out
    private val outputStreamCaptor = ByteArrayOutputStream()
}
