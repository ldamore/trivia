package com.adaptionsoft.games.trivia

import java.util.*

class RecordRandom: Random() {
    val randoms = mutableListOf<Int>()

    override fun nextInt(bound: Int): Int {
        val nextInt = super.nextInt(bound)
        randoms.add(nextInt)
        return nextInt
    }
}