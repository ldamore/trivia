package com.adaptionsoft.games.trivia.runner

import com.adaptionsoft.games.uglytrivia.Game
import java.util.*

object GameRunner {
    var notAWinner: Boolean = false
}

fun main(args: Array<String>) {
    runMain(Random())
}

fun runMain(random: Random) {
    val aGame = Game()
    aGame.add("Chet")
    aGame.add("Pat")
    aGame.add("Sue")
    do {
        aGame.roll(random.nextInt(5) + 1)
        if (random.nextInt(9) == 7) {
            GameRunner.notAWinner = aGame.wrongAnswer()
        } else {
            GameRunner.notAWinner = aGame.wasCorrectlyAnswered()
        }
    } while (GameRunner.notAWinner)
}
