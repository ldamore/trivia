package com.adaptionsoft.games.uglytrivia

import java.util.*

class Game {
    private var players = ArrayList<Any>()
    private var places = IntArray(6)
    private var purses = IntArray(6)
    private var inPenaltyBox = BooleanArray(6)

    private var popQuestions = LinkedList<Any>()
    private var scienceQuestions = LinkedList<Any>()
    private var sportsQuestions = LinkedList<Any>()
    private var rockQuestions = LinkedList<Any>()

    private var currentPlayer = 0
    private var isGettingOutOfPenaltyBox: Boolean = false

    init {
        for (i in 0..49) {
            popQuestions.addLast("Pop Question $i")
            scienceQuestions.addLast("Science Question $i")
            sportsQuestions.addLast("Sports Question $i")
            rockQuestions.addLast("Rock Question $i")
        }
    }

    fun add(playerName: String) {
        players.add(playerName)
        println("$playerName was added")
        println("They are player number ${players.size}")
        places[players.size] = 0
        purses[players.size] = 0
        inPenaltyBox[players.size] = false
    }

    fun roll(roll: Int) {
        println("${players[currentPlayer]} is the current player")
        println("They have rolled a $roll")
        if (inPenaltyBox[currentPlayer]) {
            if (isOdd(roll)) {
                isGettingOutOfPenaltyBox = true
                places[currentPlayer] = places[currentPlayer] + roll
                println("${players[currentPlayer]} is getting out of the penalty box")
                if (places[currentPlayer] > 11) places[currentPlayer] = places[currentPlayer] - 12
                println("${players[currentPlayer]}'s new location is ${places[currentPlayer]}")
                println("The category is ${currentCategory()}")
                askQuestion()
            } else {
                println("${players[currentPlayer]} is not getting out of the penalty box")
                isGettingOutOfPenaltyBox = false
            }
        } else {
            places[currentPlayer] = places[currentPlayer] + roll
            if (places[currentPlayer] > 11) places[currentPlayer] = places[currentPlayer] - 12
            println("${players[currentPlayer]}'s new location is ${places[currentPlayer]}")
            println("The category is " + currentCategory())
            askQuestion()
        }
    }

    private fun isOdd(roll: Int) = roll % 2 != 0

    private fun askQuestion() {
        if (currentCategory() === "Pop")
            println(popQuestions.removeFirst())
        if (currentCategory() === "Science")
            println(scienceQuestions.removeFirst())
        if (currentCategory() === "Sports")
            println(sportsQuestions.removeFirst())
        if (currentCategory() === "Rock")
            println(rockQuestions.removeFirst())
    }

    private fun currentCategory(): String {
        return when {
            places[currentPlayer] == 0 -> "Pop"
            places[currentPlayer] == 4 -> "Pop"
            places[currentPlayer] == 8 -> "Pop"
            places[currentPlayer] == 1 -> "Science"
            places[currentPlayer] == 5 -> "Science"
            places[currentPlayer] == 9 -> "Science"
            places[currentPlayer] == 2 -> "Sports"
            places[currentPlayer] == 6 -> "Sports"
            places[currentPlayer] == 10 -> "Sports"
            else -> "Rock"
        }
    }

    fun wasCorrectlyAnswered(): Boolean {
        if (inPenaltyBox[currentPlayer]) {
            if (isGettingOutOfPenaltyBox) {
                println("Answer was correct!!!!")
                purses[currentPlayer]++
                println("${players[currentPlayer]} now has ${purses[currentPlayer]} Gold Coins.")
                val winner = didPlayerWin()
                currentPlayer++
                if (currentPlayer == players.size) currentPlayer = 0
                return winner
            } else {
                currentPlayer++
                if (currentPlayer == players.size) currentPlayer = 0
                return true
            }
        } else {
            println("Answer was corrent!!!!")
            purses[currentPlayer]++
            println("${players[currentPlayer]} now has ${purses[currentPlayer]} Gold Coins.")
            val winner = didPlayerWin()
            currentPlayer++
            if (currentPlayer == players.size) currentPlayer = 0
            return winner
        }
    }

    fun wrongAnswer(): Boolean {
        println("Question was incorrectly answered")
        println("${players[currentPlayer]} was sent to the penalty box")
        inPenaltyBox[currentPlayer] = true
        currentPlayer++
        if (currentPlayer == players.size) currentPlayer = 0
        return true
    }

    private fun didPlayerWin() = purses[currentPlayer] != 6
}